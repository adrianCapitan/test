# README #

The project is generated using yeoman

### How do I get set up? ###

* Clone repository


```
#!cli
git clone https://adrianCapitan@bitbucket.org/adrianCapitan/test.git

```

* Bower and npm install


```
#!cli

bower install
```

```
#!cli

npm install
```


* Run application


```
#!cli

grunt serve
```


* Run tests

```
#!cli

grunt test
```