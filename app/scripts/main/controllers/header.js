'use strict';

angular.module('vending.machine').controller('HeaderCtrl', function ($scope,$location) {
  $scope.menuItems = [{
    label: 'Home',
    link: '/'
  }, {
    label: 'Unlimited',
    link: '/unlimited'
  }, {
    label: 'Limited',
    link: '/limited'
  }];


  $scope.menuItems.forEach(function (item) {
    if(item.link === $location.path()) {
      $scope.activeItem = item;
    }
  });

  $scope.changeRoute = function (item) {
    $scope.activeItem = item;
  };
});
