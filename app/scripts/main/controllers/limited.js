'use strict';

angular.module('vending.machine').controller('LimitedCtrl', function (LimitedExchange, Coin, CoinStore, $http) {
  this.coinStore = new CoinStore();
  this.coinStoreReady = false;
  $http.get('data/coin-store.json').then(function (res) {
    var coins = [];
    res.data.forEach(function (element) {
      coins.push({
        coin: new Coin(element.type),
        quantity: element.quantity
      });

    }, this);
    this.coinStore.populateWithDefaultValues(coins);
    this.coinStoreReady = true;
  }.bind(this));

  var exchange = new LimitedExchange(this.coinStore);
  this.amount = 0;
  this.error = {
    show: false,
    msg: ''
  };

  this.doExchange = function () {
    if (!this.coinStoreReady) {
      this.error.show = true;
      this.error.msg = 'Loading coin store please wait';
      return;
    }
    try {
      this.change = exchange.giveChange(this.amount);
    } catch (e) {
      this.error.show = true;
      this.error.msg = e.message;
      throw e;
    }
  };
});
