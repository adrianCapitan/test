'use strict';

angular.module('vending.machine').controller('UnlimitedCtrl',function (Exchange) {
  var exchange = new Exchange();
  this.amount = 0;
  this.error = {
    show: false,
    msg:''
  };

  this.doExchange = function () {
    try {
      this.change = exchange.giveChange(this.amount);
    }catch (e) {
      this.error.show = true;
      this.error.msg = e.message;
    }
  };
});
