'use strict';

angular.module('vending.machine').factory('Coin', function () {
  function Coin(type) {
    this.type = type;
    switch (type) {
      case Coin.POUND:
        this.value = 100;
        break;
      case Coin.FIFTY_PENCE:
        this.value = 50;
        break;
      case Coin.TWENTY_PENCE:
        this.value = 20;
        break;
      case Coin.TEN_PENCE:
        this.value = 10;
        break;
      case Coin.FIVE_PENCE:
        this.value = 5;
        break;
      case Coin.TWO_PENCE:
        this.value = 2;
        break;
      case Coin.ONE_PENNY:
        this.value = 1;
        break;
      default:
        throw new Error('Unknown coin type');
    }
  }

  Coin.POUND = 'POUND';
  Coin.FIFTY_PENCE = 'FIFTY_PENCE';
  Coin.TWENTY_PENCE = 'TWENTY_PENCE';
  Coin.TEN_PENCE = 'TEN_PENCE';
  Coin.FIVE_PENCE = 'FIVE_PENCE';
  Coin.TWO_PENCE = "TWO_PENCE";
  Coin.ONE_PENNY = "ONE_PENNY";

  Coin.getSortedAvailableCoins = function () {
    return [
      new Coin(Coin.POUND),
      new Coin(Coin.FIFTY_PENCE),
      new Coin(Coin.TWENTY_PENCE),
      new Coin(Coin.TEN_PENCE),
      new Coin(Coin.FIVE_PENCE),
      new Coin(Coin.TWO_PENCE),
      new Coin(Coin.ONE_PENNY)
    ];
  };

  return Coin;
});
