'use strict';

angular.module('vending.machine', [
  'ngRoute',
]).config(function ($routeProvider) {
  $routeProvider
    .when('/', {
      templateUrl: '/views/main/main.html',
      controller: 'MainCtrl',
      controllerAs: 'main'
    })
    .when('/unlimited', {
      templateUrl: '/views/main/unlimited.html',
      controller: 'UnlimitedCtrl',
      controllerAs: 'vm'
    })
    .when('/limited', {
      templateUrl: '/views/main/limited.html',
      controller: 'LimitedCtrl',
      controllerAs: 'vm'
    })
    .otherwise({
      redirectTo: '/'
    });
});
