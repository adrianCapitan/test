'use strict';

angular.module('vending.machine').factory('CoinStore', function (Coin) {

  function CoinStore() {
    this.coins = {};
  }

  CoinStore.prototype.addCoin = function (coin) {
    if (!this.coins[coin.type]) {
      this.coins[coin.type] = {
        coin: coin,
        quantity: 0
      };
    }
    this.coins[coin.type].quantity++;
  };

  CoinStore.prototype.addCoins = function (coin, quantity) {
    if (isNaN(parseInt(quantity))) {
      throw Error('Provided quantity is not integer');
    }
    if (!this.coins[coin.type]) {
      this.coins[coin.type] = {
        coin: coin,
        quantity: 0
      };
    }
    this.coins[coin.type].quantity += quantity;
  };

  CoinStore.prototype.populateWithDefaultValues = function (coins) {
    coins.forEach(function (element) {
      this.addCoins(element.coin, element.quantity);
    }, this);
  };

  CoinStore.prototype.canRemoveCoin = function (coin) {
    return !this.coins[coin.type] || this.coins[coin.type].quantity > 0;
  };

  CoinStore.prototype.removeCoin = function (coin) {
    if (!this.canRemoveCoin(coin)) {
      throw new Error('Insufficient coinage');
    }
    this.coins[coin.type].quantity--;
  };

  return CoinStore;
});
