'use strict';

angular.module('vending.machine').factory('Exchange', function (Coin) {

  var availableCoins = Coin.getSortedAvailableCoins();

  function Exchange() {

  }

  Exchange.prototype.canGiveCoin = function (coin) {
    return this.left.dividedBy(coin.value).greaterThanOrEqualTo(1)
  }


  Exchange.prototype.parseAmount = function (amount) {
    if(Number(amount) !== amount && amount % 1 !== 0) {
      throw new Error('Invalid amount provided');
    }
    return new BigNumber(amount);
  };



  Exchange.prototype.giveChange = function (amount) {
    var parsedAmount = this.parseAmount(amount);
    var change = {};
    this.left = parsedAmount;

    if (this.left.toNumber() % 1 != 0) {
      throw new Error('Invalid amount provided');
    }
    for (var i = 0; i < availableCoins.length || this.left.greaterThan(0); i++) {
      while (this.canGiveCoin(availableCoins[i]) && this.left.greaterThan(0)) {
        if (!change[availableCoins[i].type]) {
          change[availableCoins[i].type] = availableCoins[i];
          change[availableCoins[i].type].quantity = 0;
        }
        change[availableCoins[i].type].quantity++;
        this.left = this.left.minus(availableCoins[i].value);
      }
    };

    return change;
  }

  return Exchange;
});
