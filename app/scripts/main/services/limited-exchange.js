'use strict';

angular.module('vending.machine').factory('LimitedExchange', function (Exchange,Coin) {

  function LimitedExchange(coinStore) {
    this.coinStore = coinStore;
    this.removedCoins = [];
  }

  LimitedExchange.prototype = new Exchange();
  LimitedExchange.prototype.constructor = LimitedExchange;

  LimitedExchange.prototype.canGiveCoin = function (coin) {
    var canGiveCoin = this.left.dividedBy(coin.value).greaterThanOrEqualTo(1);
    if(canGiveCoin) {
      try {
        this.coinStore.removeCoin(coin);
        this.removedCoins.push(coin);
      } catch(e) {
        if(coin.type==Coin.ONE_PENNY) {
          throw e;
        }
        return false;
      }
    }
    return canGiveCoin;
  };

  var giveChange = LimitedExchange.prototype.giveChange;

  LimitedExchange.prototype.giveChange = function (amount) {
    try {
      this.removedCoins = [];
      return giveChange.call(this,amount);
    }catch (e) {
      this.removedCoins.forEach(function (coin) {
        this.coinStore.addCoin(coin);
      },this);
      throw e;
    }
  };

  return LimitedExchange;
});
