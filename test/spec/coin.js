'use strict';

describe('Model: Coin', function () {

  var coin;
  var Coin;

  beforeEach(angular.mock.module('vending.machine'));

  beforeEach(inject(function(_Coin_) {
    Coin = _Coin_;
    coin = new _Coin_(_Coin_.TEN_PENCE);
  }));

  it('should create a coin', function () {
    expect(coin.type).toBe(Coin.TEN_PENCE);
  });

  it('should get a list of all available coin instances', function () {
    expect(Coin.getSortedAvailableCoins().length).toBe(7);
  });

  it('should get a list of all available coin instances', function () {
    expect(Coin.getSortedAvailableCoins().length).toBe(7);
  });
})
;
