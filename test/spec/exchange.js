'use strict';

describe('Factory: Exchange', function () {

  var exchange;

  beforeEach(angular.mock.module('vending.machine'));

  beforeEach(inject(function(_Exchange_) {
    exchange = new _Exchange_();
  }));

  it('should give change for specified amount', function () {
    var amount = 1526;
    var change = exchange.giveChange(amount);
    var sum = 0;
    for(var key in change) {
      sum +=change[key].value*change[key].quantity;
    }
    expect(sum).toBe(amount);
  });

  it('should throw error for numbers with digits', function () {
    var amount = 10.222;
    expect(function () {
      exchange.giveChange(amount);
    }).toThrow(new Error('Invalid amount provided'));
  });

  it('should throw error for non numeric values', function () {
    var amount = 'Test';
    expect(function () {
      exchange.giveChange(amount);
    }).toThrow(new Error('Invalid amount provided'));
  });

});
