'use strict';


describe('Factory: LimitedExchange', function () {

  var LimitedExchange;
  var CoinStore;
  var Coin;

  fixture.setBase('app/data');

  function createLimitedExchange() {
    var coinStore = new CoinStore();
    var coins = [];
    fixture.load('coin-store.json').forEach(function (element) {
      coins.push({
        coin: new Coin(element.type),
        quantity: element.quantity
      });
    });
    coinStore.populateWithDefaultValues(coins);
    return new LimitedExchange(coinStore);
  }

  beforeEach(angular.mock.module('vending.machine'));

  beforeEach(inject(function (_LimitedExchange_, _CoinStore_,_Coin_) {
    Coin = _Coin_;
    LimitedExchange = _LimitedExchange_;
    CoinStore = _CoinStore_;
  }));

  it('should create a limited exchange', function () {
    expect(createLimitedExchange()).toBeDefined();
  });

  it('should give change for specified amount', function () {
    var exchange = createLimitedExchange();
    var amount = 854;
    var change = exchange.giveChange(amount);
    var sum = 0;
    for (var key in change) {
      sum += change[key].value * change[key].quantity;
    }
    expect(sum).toBe(amount);
  });

  it('should throw error for numbers with digits', function () {
    var exchange = createLimitedExchange();
    var amount = 10.222;
    expect(function () {
      exchange.giveChange(amount);
    }).toThrow(new Error('Invalid amount provided'));
  });

  it('should throw error for non numeric values', function () {
    var exchange = createLimitedExchange();
    var amount = 'Test';
    expect(function () {
      exchange.giveChange(amount);
    }).toThrow(new Error('Invalid amount provided'));
  });

  it('should throw error for Insufficient coinage', function () {
    var exchange = createLimitedExchange();
    var amount = 10000000000;
    expect(function () {
      exchange.giveChange(amount);
    }).toThrow(new Error('Insufficient coinage'));
  });

});
